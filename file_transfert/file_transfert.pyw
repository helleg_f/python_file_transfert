from transfert.gui import Gui
from transfert.client import Client
from transfert.server import Server
import asyncio
import threading

def main():
    port = 4242
    eventLoop = asyncio.get_event_loop()
    client = Client(port, eventLoop)
    server = Server(port, eventLoop)
    gui = Gui(client, server)
    eventLoop.run_until_complete(gui.update_routine())
    client.close()
    server.close()
    eventLoop.close()

main()

