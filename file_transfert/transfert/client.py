import socket
import asyncio
    
import tkinter.filedialog as filedialog

class Client:

    def __init__(self, port, eventLoop, buffSize = 4096):
        self._port = port
        self._eventLoop = eventLoop
        self._buffSize = buffSize
        self._connection = None
        self._list = []
        self._disconnect = False
        self.updateStatus = print
        self.updateList = print
        self.toDownload = None # TODO: replace with function

    def connect(self, addr):
        if (self._connection is None):
            self._connection = self._eventLoop.create_task(self._connect_routine(addr))
            self._connection.add_done_callback(self._disconnected)
            
    def close(self):
        pass

    async def _connect_routine(self, addr):
        self._disconnect = False
        self.updateStatus("connecting...")
        reader, writer = await asyncio.open_connection(addr, self._port)
        self._connection.add_done_callback(lambda: writer.close())
        self.updateStatus("connected to " + addr)
        while (not self._disconnect):
            await self._request_list(reader, writer)
            for i in range(10):
                await asyncio.sleep(0.5)
                if (self.toDownload is not None):
                    file = self.toDownload
                    self.toDownload = None
                    await self._request_get(reader, writer, file)
        self.updateStatus("disconnected")

    def _disconnected(self, future):
        e = future.exception()
        if (e is not None):
            self.updateStatus("error " + str(e))
        else:
            self.updateStatus("disconnected")
        self._connection = None

    async def _request_list(self, reader, writer):
        list = []
        writer.write(b"LIST\n")
        line = await reader.readline()
        while (line != b"\n"):
            list.append(line.decode()[:-1])
            line = await reader.readline()
        if (list != self._list):
            self._list = list
            self.updateList(list)

    async def _request_get(self, reader, writer, fileName):
        file = filedialog.asksaveasfile(mode="wb", initialfile=fileName)
        if (file is None):
            return
        line = "GET {}\n".format(fileName).encode()
        writer.write(line)
        line = await reader.readline()
        if (line == b"ko\n"):
            self.updateStatus("failed to download " + fileName)
            return
        line = await reader.readline()
        size = int(line.decode()[:-1])
        with file:
            toWrite = size
            while (toWrite > 0):
                self.updateStatus("receiving {}, {} bytes left".format(fileName, toWrite))
                n = min(self._buffSize, toWrite)
                data = await reader.read(n)
                toWrite -= len(data)
                file.write(data)
            self.updateStatus(fileName + " downloaded")
        