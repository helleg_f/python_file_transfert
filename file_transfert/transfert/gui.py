import asyncio
from tkinter import *
import tkinter.filedialog as filedialog
import tkinter.messagebox as messagebox
import socket
from transfert import client, server


class GuiServer:
    
    def __init__(self, root, server):
        self._root = root
        self._addressesFrame = None
        self._filesFrame = None
        self._files = {}
        self._status = StringVar()
        self._statusLabel = Label(root, textvariable=self._status)
        #self._server = server

        Label(root, text="server").grid(row=0, column=0, columnspan=2)
        Label(root, text="address:").grid(row=1, column=0, sticky=NW)
        Button(root, text="update",
            command=self.update_addresses).grid(row=2, columnspan=2)
        self.update_addresses()
        Button(root, text="add file",
            command=self.add_file).grid(row=4, column=0)
        Button(root, text="open server", bg="blue", fg="white",
            command=server.accept).grid(row=4, column=1)
        self._statusLabel.grid(row=5, column=0, columnspan=2)
        server.updateStatus = lambda s: self._status.set(s)
        server.files = self._files

    def update_addresses(self):
        newFrame = Frame(self._root)
        newFrame.grid(row=1, column=1, sticky=W)
        r = 0
        for addr in socket.gethostbyname_ex("")[2]:
            Label(newFrame, text=addr).grid(row=r, column=0, sticky=W)
            r += 1
        if self._addressesFrame is not None:
            self._addressesFrame.destroy()
        self._addressesFrame = newFrame

    def update_files(self):
        def remove(name):
            self._files.pop(name)
            self.update_files()
        newFrame = Frame(self._root)
        newFrame.grid(row=3, columnspan=2, sticky=W)
        r = 0
        for name, file in self._files.items():
            rm = lambda n=name: remove(n)
            Label(newFrame, text=name).grid(row=r, column=0, sticky=W)
            Button(newFrame, text="X", bg="red",
                command=rm).grid(row=r, column=1, sticky=E)
            r += 1
        if self._filesFrame is not None:
            self._filesFrame.destroy()
        self._filesFrame = newFrame

    def add_file(self):
        for path in filedialog.askopenfilenames():
            name = path.split("/")[-1]
            old = self._files.get(name)
            if old is not None:
                if (old != path):
                    answer = messagebox.askyesnocancel("add file",
                               "% already in list, replace it ?" % name)
                    if answer is None:
                        break
                    elif answer == "no":
                        continue
                else:
                    continue
            self._files[name] = path
        self.update_files()


class GuiClient:

    def __init__(self, root, client):
        self._root = root
        self._client = client
        self._addrText = StringVar()
        self._status = StringVar()
        self._statusLabel = Label(root, textvar=self._status)
        self._connectButton = Button(root, bg="blue", fg="white",
                                text="connect", command=self.connect)
        self._listFrame = None

        Label(root, text="client").grid(row=0, column=0, columnspan=2)
        Label(root, text="server address:").grid(row=1, column=0, sticky=W)
        Entry(root, textvariable=self._addrText).grid(row=1, column=1, sticky=W)
        self._addrText.set("localhost")
        self._connectButton.grid(row=2, columnspan=2)
        self._statusLabel.grid(row=4, columnspan=2)
        client.updateStatus = lambda s: self._status.set(s)
        client.updateList = self.update_list
        
    def connect(self):
        self._client.connect(self._addrText.get())
        
    def update_list(self, list):
        newFrame = Frame(self._root)
        newFrame.grid(row=3, columnspan=2)
        r = 0
        def download(file):
            self._client.toDownload = file
        for file in list:
            Button(newFrame, text=file,
                    command=lambda f=file: download(f)).grid(row=r)
            r += 1
        if self._listFrame is not None:
            self._listFrame.destroy()
        self._listFrame = newFrame


class Gui:
    
    def __init__(self, client, server):
        self.root = Tk()
        self.root.title("file transfert")
        leftFrame = Frame(self.root)
        rightFrame = Frame(self.root)
        leftFrame.grid(row=0, column=0, padx=10, sticky=N)
        rightFrame.grid(row=0, column=1, padx=10, sticky=N)
        self.server = GuiServer(leftFrame, server)
        self.client = GuiClient(rightFrame, client)

    async def update_routine(self, sleep = 0.05):
        try:
            while (True):
                self.root.update()
                await(asyncio.sleep(sleep))
        except TclError as e:
            pass
            