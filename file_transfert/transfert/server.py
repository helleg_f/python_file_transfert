import socket
import asyncio
import re

class Server:

    def __init__(self, port, eventLoop, buffSize=4096):
        self._port = port
        self._eventLoop = eventLoop
        self._buffSize = buffSize
        self.coroutine = None # remove ?
        self._connection = None
        self._commands = {re.compile("LIST\r?\n"): self.request_list,
                        re.compile("GET .*\r?\n"): self.request_get}
        self.files = {}
        self.updateStatus = print

    def close(self):
        if (self.coroutine is not None):
            self.coroutine.close()

    def accept(self):
        if (self.coroutine is None):
            self.updateStatus("starting server with port " + str(self._port))
            self.coroutine = asyncio.start_server(self.handle_client,
                                                    port=self._port)
            self._connection = self._eventLoop.create_task(self.coroutine)
        else:
            self.updateStatus("server ready")

    async def handle_client(self, reader, writer):
        invalidRequestLimit = 20
        while (invalidRequestLimit > 0):
            self.updateStatus("waiting request...")
            line = await reader.readline()
            if (line == b""):
                break
            request = line.decode()
            for k in self._commands:
                if re.fullmatch(k, request):
                    await self._commands[k](request, reader, writer)
                    break
            else:
                writer.write(b"ko\n")
                invalidRequestLimit -= 1
        self.updateStatus("client disconnected")
        writer.close()
        
    async def request_list(self, request, reader, writer):
        self.updateStatus("sending file list...")
        for k in self.files:
            writer.write("{}\n".format(k).encode())
        writer.write(b"\n")
        await writer.drain()

    async def request_get(self, request, reader, writer):
        if (request[-2:] == "\r\n"):
            name = request[4:-2]
        else:
            name = request[4:-1]
        path = self.files.get(name)
        if (path is not None):
            try:
                with open(path, mode="rb") as file:
                    size = file.seek(0, 2)
                    file.seek(0, 0)
                    writer.write(b"ok\n")
                    writer.write("{}\n".format(size).encode())
                    toRead = size
                    while (toRead > 0):
                        self.updateStatus("sending {}, {} bytes left".format(name, toRead))
                        n = min(self._buffSize, toRead)
                        data = file.read(n)
                        toRead -= len(data)
                        writer.write(data)
                        await writer.drain()
                self.updateStatus("sent " + name)
                return
            except IOError:
                self.updateStatus("failed to open " + name)
        writer.write(b"ko\n")
